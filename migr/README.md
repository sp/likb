```
java -jar target/migr.jar --spring.profiles.active=PROM
```

## [85.5.2 Execute Liquibase Database Migrations on Startup](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html#howto-execute-liquibase-database-migrations-on-startup)

To automatically run Liquibase database migrations on startup, add the `org.liquibase:liquibase-core` to your classpath.

By default, the master change log is read from `db/changelog/db.changelog-master.yaml`, but you can change the location
by setting spring.liquibase.change-log. In addition to YAML, Liquibase also supports JSON, XML, and SQL change log formats.

By default, Liquibase autowires the (`@Primary`) DataSource in your context and uses that for migrations.
If you need to use a different DataSource, you can create one and mark its `@Bean` as `@LiquibaseDataSource`.
If you do so and you want two data sources, remember to create another one and mark it as `@Primary`.
Alternatively, you can use Liquibase’s native DataSource by setting `spring.liquibase.[url,user,password]` in external properties.
Setting either `spring.liquibase.url` or `spring.liquibase.user` is sufficient to cause Liquibase to use its own DataSource.
If any of the three properties has not be set, the value of its equivalent spring.datasource property will be used.

See LiquibaseProperties for details about available settings such as contexts, the default schema, and others.

There is a Liquibase sample so that you can see how to set things up.
