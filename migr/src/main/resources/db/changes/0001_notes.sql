--liquibase formatted sql
--changeset sa:1

CREATE TABLE notes (
    id IDENTITY,
    note VARCHAR
);
